import "../css/header.css";
import logo from "../assets/images/Logo-color.jpg";
import { Link, useLocation } from "react-router-dom";
import { AnimateSharedLayout, motion } from "framer-motion";
import { useState } from "react";
import React, { Component } from 'react';

const Header = ({
  transparent = false,
  float = false,
  initial,
  animate,
  exit,
  transition,
}) => {
  const { pathname } = useLocation();
  const [hover, setHover] = useState(pathname);

  const spring = {
    type: "spring",
    stiffness: 1000,
    damping: 200,
  };

  const home = pathname === "/";

  return (
    <motion.div
      initial={initial}
      animate={animate}
      exit={exit}
      transition={transition}
      className="header-container"
      style={float ? { position: "absolute" } : undefined}
    >
      <div
        className="header-menu"
        style={transparent ? { backgroundColor: "#943854" } : undefined}
      >
        <div className="header-left">
          {transparent ? (
            <Link to="/">
              <img src={logo} alt="logo" width="60" />
            </Link>
          ) : (
            <Link to="/">
              <img src={logo} alt="logo" width="60" />
            </Link>
          )}
        </div>

        <div
          className="header-right"
          style={transparent ? { color: "white" } : { color: "#7e3c80" }}
        >
            <CreateLink
              path="/"
              isSelected={pathname === "/"}
              isHover={hover === "/"}
              label="Accueil"
              onMouseEnter={() => setHover("/")}
              onMouseOut={() => setHover(pathname)}
            />
            <CreateLink
              path="/about"
              isSelected={pathname === "/services"}
              isHover={hover === "/services"}
              label="Nos services"
              onMouseEnter={() => setHover("/services")}
              onMouseOut={() => setHover(pathname)}
            />
            <CreateLink
              path="/contact"
              isSelected={pathname === "/contact"}
              isHover={hover === "/contact"}
              label="Contact"
              onMouseEnter={() => setHover("/contact")}
              onMouseOut={() => setHover(pathname)}
            />
        </div>
      </div>
    </motion.div>
  );

  function CreateLink({
    path,
    isSelected,
    isHover,
    label,
    onMouseEnter,
    onMouseOut,
  }) {
    return (
      <Link
        style={{
          display: "flex",
        }}
        to={path}
        onMouseEnter={onMouseEnter}
        onMouseOut={onMouseOut}
      >
        <motion.div
          className="navLink-header"
          initial={false}
          animate={
            isHover
              ? {
                  borderTop: home ? "5px solid white" : "5px solid #803d80",
                  marginBottom: "5px",
                }
              : undefined
          }
          transition={{ spring }}
        >
          <span
            style={{
              color: !home ? "#803d80" : "white",
            }}
          >
            {label}
          </span>
        </motion.div>
      </Link>
    );
  }
};

export default Header;

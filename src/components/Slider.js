import React, { Component } from 'react';
import {
  createRef,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
const styles = {
  container: {
    width: "100%",
    height: "100%",
    position: "relative",
    overflow: "hidden",
  },
  section: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
};
export const SliderContainer = forwardRef(({ value = 0, children }, ref) => {
  const [currentIndex, setCurrentIndex] = useState(value);
  const containerRef = createRef();

  useImperativeHandle(ref, () => ({
    childrenNumber: () => {
      return children.length;
    },
    setIndex: (index) => setCurrentIndex(index),
    next: () => {
      setCurrentIndex((currentIndex + 1) % children.length);
      const index = (currentIndex + 1) % children.length;
      return index;
    },
    previous: () => {
      const aroundIndex = () => {
        let temp = currentIndex - 1;
        if (temp < 0) {
          return children.length + temp;
        } else {
          return temp;
        }
      };
      setCurrentIndex(aroundIndex());
      return aroundIndex();
    },
  }));

  useEffect(() => {
    containerRef.current.scroll({
      top: currentIndex * containerRef.current.offsetHeight,
      behavior: "smooth",
    });
  }, [containerRef, currentIndex, value]);

  return (
    <div style={styles.container} ref={containerRef}>
      {children}
    </div>
  );
});

export const SliderSection = ({ index, children, style }) => {
  return (
    <div style={{ ...styles.section, top: `${100 * index}%`, ...style }}>
      {children}
    </div>
  );
};

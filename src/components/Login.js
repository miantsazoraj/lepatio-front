import { Container } from "react-bootstrap";
import Header from "./Header";
import React, { Component } from "react";
import Footer from "./Footer";
import Landing from "./Landing";

const Home = () => {
  return (
    <Container fluid className="p-0 m-0">
      <Header show transparent float />
      <div class="register-right-pane container-fluid d-flex flex-column">
        <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
          <div class="scrollable-content col-sm-7 col-lg-6 col-xl-6 py-6 py-lg-0 position-absolute h-100 top-0 right-0">
            <div class="row justify-content-center">
              <div class="col-11 col-lg-10 col-xl-10">
                  <form>
                    <div
                        ref={(node) => (this.componentRef.last_name = node)}
                        class="form-group"
                    >
                        <label class="form-control-label">Nom de famille*</label>
                        <div class="input-group">
                        <input
                            type="text"
                            name="last_name"
                            onChange={this.changePerson}
                            value={this.state.applicant.person.last_name}
                            placeholder="ex. Rakotomalala"
                        />
                        </div>
                        {this.renderError("data.relationships[0].last_name")}
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Home;

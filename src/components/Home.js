import { Container } from "react-bootstrap";
import Header from "./Header";
import React, { Component } from "react";
import Footer from "./Footer";
import Landing from "./Landing";

const Home = () => {
  return (
    <Container fluid className="p-0 m-0">
      <Header show transparent float />
      <Landing />
      <Footer />
    </Container>
  );
};

export default Home;

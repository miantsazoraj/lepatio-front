import React from "react";
import up from "../assets/images/up.png";
import down from "../assets/images/down.png";
import "../css/navigation.css";

const Navigation = ({
  activeIndex,
  number = 1,
  hideUp = false,
  hideDown = false,
  onClickDown,
  onClickUp,
  dots = false,
  upIcon = up,
  downIcon = down,
  size = "25px",
  gap = 0,
  styles,
}) => {
  return (
    <div className="container-navigation" style={styles}>
      <img
        className="up"
        style={{ width: size, opacity: hideUp ? "0" : "1" }}
        src={upIcon}
        alt="up"
        onClick={onClickUp}
      />

      {dots && (
        <div>
          {[...new Array(number)].map((_, index) => (
            <div
              key={index}
              className={`circle-navigation${
                activeIndex === index ? " active" : ""
              }`}
            ></div>
          ))}
        </div>
      )}

      <img
        className="down"
        style={{ width: size, opacity: hideDown ? "0" : "1" }}
        src={downIcon}
        alt="down"
        onClick={onClickDown}
      />
    </div>
  );
};

export default Navigation;

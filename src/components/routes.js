import { AnimatePresence, motion } from "framer-motion";
import React from "react";
import { Switch } from "react-router";
import { BrowserRouter as Router, Route } from "react-router-dom";
// eslint-disable-next-line no-unused-vars
import { RouteProps } from "react-router-dom";
// eslint-disable-next-line no-unused-vars
import { MotionProps } from "framer-motion";

export const AnimatedRoutes = ({ children }) => (
  <Router>
    <Route
      render={({ location }) => (
        <AnimatePresence exitBeforeEnter={false} initial={false}>
          <Switch location={location} key={location.pathname}>
            {children}
          </Switch>
        </AnimatePresence>
      )}
    />
  </Router>
);

/**
 * @param {RouteProps & MotionProps} param0
 * @returns {JSX.Element}
 */
export const AnimatedRoute = ({
  initial = false,
  animate,
  transition,
  exit = {},
  component,
  render,
  children,
  ...rest
}) => (
  <Route {...rest}>
    <motion.div {...{ initial, animate, exit, transition }}>
      <Route {...{ component, render, children }} />
    </motion.div>
  </Route>
);

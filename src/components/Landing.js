import React, { createRef } from "react";
import { Col, Row } from "react-bootstrap";
import background2 from "../assets/images/background2.jpg";
import arrowDown from "../assets/images/arrow-down.png";
import arrowDownWhite from "../assets/images/arrow-down-white-with-space.png";
import background1 from "../assets/images/background1.jpg";
import "../css/landing.css";

import Navigation from "./Navigation";
import { SliderContainer, SliderSection } from "./Slider";
import { Link } from "react-router-dom";

import { motion } from "framer-motion";

const Landing = () => {
  const sliderRef = createRef();

  const handleClickUp = () => {
    sliderRef.current.previous();
  };

  const handleClickDown = () => {
    sliderRef.current.next();
  };

  const variants = {
    rest: {
      transition: {
        duration: 2
      }
    },
    hover: {
      opacity: 0,
      transition: {
        duration: 0.4
      }
    }
  }

  const variantsWhite = {
    rest: { opacity: 0, duration: 0.2 },
    hover: {
      opacity: 1,
      transition: {
        duration: 0.4,
      }
    }
  }


  return (
    <div className="p-0 m-0 overflow-hidden">
      <div id="landing"></div>
      <Row>
        <Col>
          <div className="landing">
            <SliderContainer ref={sliderRef}>
              <SliderSection index={0}>
                <div>
                  <img
                    src={background1}
                    className="background-image"
                    alt=""
                  />
                  <motion.div
                    initial={{ height: "100vh", opacity: 0, scale: 2 }}
                    animate={{
                      scale: 1,
                      opacity: 1,
                    }}
                    transition={{ duration: 0.6, bounce: 0, delay: 0.4 }}
                  >
                    <h3 className="text-landing-2">
                      Le <span className="patio"> Patio </span> <br /> {" "}
                    </h3>
                  </motion.div>
                </div>
              </SliderSection>
              <SliderSection index={1}>
                {" "}
                <div>
                  <img
                    src={background2}
                    className="background-image"
                    alt=""
                  />
                  <h3 className="text-landing-2">
                    Le <span className="patio"> Patio </span> <br /> {" "}
                  </h3>
                </div>
              </SliderSection>
            </SliderContainer>
            <div className="navigation">
              <Navigation
                number={2}
                onClickUp={handleClickUp}
                onClickDown={handleClickDown}
                dots={true}
              />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Landing;

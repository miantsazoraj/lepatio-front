import "../css/footer.css";
import logo from "../assets/images/Logo-color.jpg";
import { Col, Row } from "react-bootstrap";
import logoFacebook from "../assets/images/facebook.png";
import { Link } from "react-router-dom";
import React, { Component } from "react";

const Footer = () => {
  return (
    <div className="footer-container">
      <div className="upper-section">
        <Row
          className="p-0 m-0 w-100"
          style={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Col className="p-0 m-0">
            <div className="footer-section middle">
              <Link to="#" className="menu-item-footer">
                Nos Services
              </Link>
              <hr />
              <ul>
                <li className="menu-list">
                  <Link to="/">Salle de fête</Link>
                </li>
                <li className="menu-list">
                  <Link to="/">Chambre d'hôtes</Link>
                </li>
                <li className="menu-list">
                  <Link to="/">Appartements</Link>
                </li>
              </ul>
            </div>
          </Col>
          <Col className="p-0 m-0">
            <div className="footer-section middle">
              <img style={{ width: "130px" }} src={logo} alt="Le Patio" />
            </div>
            <div className="lower-section">
              <span>© {new Date().getFullYear()} Le Patio</span>
              <span>Tous droits réservés</span>
            </div>
          </Col>
          <Col className="p-0 m-0">
            <div className="footer-section middle">
              <Link to="#" className="menu-item-footer">
                Contact
              </Link>
              <hr />
              <ul>
                <li className="menu-list">contact@lepatio.com</li>
                <li>
                  <Link to="https://www.facebook.com/LePatio">
                    <img
                      style={{ width: "20px", marginRight: "4px" }}
                      src={logoFacebook}
                      alt="facebook"
                    />
                  </Link>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Footer;

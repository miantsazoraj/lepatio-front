import './App.css';
import { AnimatedRoute } from "./components/routes";
import Home from "./components/Home";
import Login from "./components/Login";
import React, { Component } from 'react';

function App() {
  return (
    <AnimatedRoute exact path="/">
      <Login />
    </AnimatedRoute>
  );
}

export default App;
